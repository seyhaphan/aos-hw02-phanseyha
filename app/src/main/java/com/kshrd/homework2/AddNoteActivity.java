package com.kshrd.homework2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class AddNoteActivity extends AppCompatActivity {
    private Button btnSave,btnCancel;
    private EditText edTitle,edDescription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_note);
        btnCancel = findViewById(R.id.btnCancel);
        btnSave = findViewById(R.id.btnSave);
        edTitle = findViewById(R.id.edTitle);
        edDescription = findViewById(R.id.edDescription);

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = getIntent();
                intent.putExtra("TITLE",edTitle.getText().toString());
                intent.putExtra("DESCRIPTION",edDescription.getText().toString());
                setResult(RESULT_OK,intent);
                finish();
            }
        });
    }
}