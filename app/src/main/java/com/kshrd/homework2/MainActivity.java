package com.kshrd.homework2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.telecom.Call;
import android.view.View;
import android.widget.Toast;

import java.io.NotActiveException;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void mainItemClick(View view) {
        switch (view.getId()){
            case R.id.btnGallery:
                startActivity(new Intent(this,GalleryActivity.class));
                break;
            case R.id.btnContact:
                startActivity(new Intent(this,ContactActivity.class));
                break;
            case R.id.btnCall:
                startActivity(new Intent(this, CallActivity.class));
                break;
            case R.id.btnProfile:
                startActivity(new Intent(this,ProfileActivity.class));
                break;
            case R.id.btnNote:
                startActivity(new Intent(this, NoteActivity.class));
        }
    }
}