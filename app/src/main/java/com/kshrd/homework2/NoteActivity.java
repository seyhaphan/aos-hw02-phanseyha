package com.kshrd.homework2;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.GridLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class NoteActivity extends AppCompatActivity {
    private static final int REQUEST_CODE = 1;
    private TextView edTitle,edDescription;
    private GridLayout gridLayoutParent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note);
        edTitle = new TextView(this);
        edDescription = new TextView(this);
        gridLayoutParent = findViewById(R.id.grid_layout_parent);
    }

    //Add note
    public void addNote(View view) {
        startActivityForResult(new Intent(this,AddNoteActivity.class),REQUEST_CODE);
    }

    //get data from add note
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        try {
            super.onActivityResult(requestCode, resultCode, data);

            if (requestCode == REQUEST_CODE  && resultCode  == RESULT_OK) {

                //get data from add note form
                String title = data.getStringExtra("TITLE");
                String description = data.getStringExtra("DESCRIPTION");

                edTitle.setTextSize(30);
                edTitle.setTextColor(Color.WHITE);
                edTitle.setEllipsize(TextUtils.TruncateAt.END);
//                edTitle.setTypeface(null, Typeface.BOLD);

                edTitle.setText(title);
                edDescription.setText(description);

            }
        } catch (Exception ex) {
            Toast.makeText(this, ex.toString(),
                    Toast.LENGTH_SHORT).show();
        }

    }

}